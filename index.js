var canvas = document.getElementById('canvas');//畫布
var ctx = canvas.getContext("2d");//筆

//初始化
//canvas.height = window.innerHeight;
//canvas.width = window.innerWidth;


ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.lineWidth = 5;

//繪圖狀態
var isDrawing = false;
//座標
var lastX = 0;
var lastY = 0;
//筆刷模式
var tool = "pen";
var hue = 0;
//文字
var font;
var txtSize;
var words;
var texting = false;//現在不是打字狀態
//繪圖紀錄
var undoList = [];
var redoList = [];
//var step = -1;
var now = new Image();
window.onload = function() {
    this.undoList.pu
}
//undo
function Undo(){
    if(undoList.length>0){
        redoList.push(ctx.getImageData(0,0,canvas.width,canvas.height));
        var temp = undoList.pop();
        ctx.putImageData(temp,0,0);
    }
}
//redo
function Redo(){
    if(redoList.length>0){
        undoList.push(ctx.getImageData(0,0,canvas.width,canvas.height));
        var temp = redoList.pop();
        ctx.putImageData(temp,0,0);
    }
}
//上傳圖片
var Inputs = document.getElementById("file");
Inputs.addEventListener('change',function(e){
    if(e.target.files){
        var temp = ctx.getImageData(0,0,canvas.width,canvas.height);
        undoList.push(temp);
        redoList = [];
        var uploadFile = e.target.files[0];
        var Reader = new FileReader();
        Reader.readAsDataURL(uploadFile);
        Reader.onload = function(ev){
            var img = new Image();
            img.src = ev.target.result;
            img.onload = function(evn){
                ctx.clearRect(0,0,canvas.width,canvas.height);
                ctx.drawImage(img,0,0,canvas.width,canvas.height);
            }
        }
    }
});

//改變筆刷大小
let range = document.getElementById("size");

range.onchange = function(){
    ctx.lineWidth = this.value;
};


//畫筆
function Draw(e){
    if(!isDrawing)return;
    ctx.beginPath();
    ctx.moveTo(lastX,lastY);
    ctx.lineTo(e.offsetX,e.offsetY);
    ctx.stroke();
    [lastX,lastY] = [e.offsetX,e.offsetY]; 
    ctx.closePath();   
}
//畫線
function DrawLine(e){
    if(!isDrawing)return;
    //delete path
    deleteOld();
    ctx.beginPath();
    ctx.moveTo(lastX,lastY);
    ctx.lineTo(e.offsetX,e.offsetY);
    ctx.stroke();
    ctx.closePath();
}
//畫圓
function DrawCircle(e){
    if(!isDrawing)return;
    deleteOld();
    ctx.beginPath();
    ctx.arc((lastX+e.offsetX)/2,(lastY+e.offsetY)/2,Math.sqrt((lastX - e.offsetX)**2+(lastY-e.offsetY)**2)/2,0,2*Math.PI);
    ctx.stroke();
    ctx.closePath();
}
//畫方
function DrawRect(e){
    if(!isDrawing)return;
    deleteOld();
    ctx.beginPath();
    ctx.strokeRect(lastX,lastY,(e.offsetX - lastX),(e.offsetY - lastY));
    ctx.stroke();
    ctx.closePath();
}
//畫三角
function DrawTri(e){
    if(!isDrawing)return;
    deleteOld();
    ctx.beginPath();
    ctx.moveTo(lastX,lastY);
    ctx.lineTo(e.offsetX,e.offsetY);
    ctx.lineTo(e.offsetX + 2*(lastX - e.offsetX),e.offsetY);
    ctx.lineTo(lastX,lastY);
    ctx.stroke();
    ctx.closePath();
}
//橡皮擦
function Erase(e){
    if(!isDrawing)return;
    ctx.globalCompositeOperation = "destination-out";
    ctx.beginPath();
    ctx.moveTo(lastX,lastY);
    ctx.lineTo(e.offsetX,e.offsetY);
    ctx.stroke();
    [lastX,lastY] = [e.offsetX,e.offsetY]; 
    ctx.closePath();
}
//文字
function Write(e){
    if(!isDrawing)return;
    deleteOld();
    ctx.fillText(words,e.offsetX,e.offsetY);
}
//彩虹畫筆
function DrawRainbow(e){
    if(!isDrawing)return;
    ctx.strokeStyle = `hsl(${ hue }, 100%, 50%)`;	
    if(hue >= 360) hue = 0;
    hue+=5;
    ctx.beginPath();
    ctx.moveTo(lastX,lastY);
    ctx.lineTo(e.offsetX,e.offsetY);
    ctx.stroke();
    [lastX,lastY] = [e.offsetX,e.offsetY]; 
    ctx.closePath(); 
}

canvas.onmousedown = (function(e){
    //繪圖紀錄
    var temp2 = ctx.getImageData(0,0,canvas.width,canvas.height);
    undoList.push(temp2);
    redoList = [];
    ctx.save();
    //筆刷顏色
    var colour = document.getElementById("color").value;
    ctx.strokeStyle = colour;
    //文字
    words = document.getElementById("words").value;
    ctx.fillStyle = colour;
    //txtsize = document.getElementById("Fontsize").value;

    ctx.font =  txtSize+"px " + font; 

    //開啟繪圖狀態
    isDrawing = true;
    [lastX,lastY] = [e.offsetX,e.offsetY];

    if(tool == "Text"){
        ctx.fillText(words,e.offsetX,e.offsetY);
    }
});

canvas.onmousemove = (function(e){
    if(isDrawing){
        if(tool == "pen"){
            Draw(e);
        }else if(tool == "eraser"){
            Erase(e);
        }else if(tool == "line"){
            DrawLine(e);
        }else if(tool == "circle"){
            DrawCircle(e);
        }else if(tool == "Rectangle"){
            DrawRect(e);
        }else if(tool == "Triangle"){
            DrawTri(e);
        }else if(tool == "Rainbow"){
            DrawRainbow(e);
        }else if(tool == "Text"){
            Write(e);
        }
        ctx.save();
    }
});
canvas.onmouseout =( function(e){
    ctx.globalCompositeOperation = "source-over";
    isDrawing = false;
   //AddHistory();
});
canvas.onmouseup = (function(e){
    ctx.globalCompositeOperation = "source-over";
    isDrawing = false;
    ctx.closePath();
    //AddHistory();
});

//筆的型態
function changetoPen(){
    tool = "pen";
    document.getElementById("canvas").className = "pen";
}
function changetoEraser(){
    tool = "eraser";
    document.getElementById("canvas").className = "eraser";
}
function changetoLine(){
    tool = "line";
    document.getElementById("canvas").className = "shape";
}
function changetoCircle(){
    tool = "circle";
    document.getElementById("canvas").className = "shape";
}
function changetoRect(){
    tool = "Rectangle";
    document.getElementById("canvas").className = "shape";
}
function changetoTri(){
    tool = "Triangle";
    document.getElementById("canvas").className = "shape";
}
function changetoRainbow(){
    tool = "Rainbow";
    document.getElementById("canvas").className = "rainbow";
    hue = 0;
}
function changetoText(){
    tool = "Text";
    document.getElementById("canvas").className = "text";
}

function changeFont(val){
    font = val;
}

function changeFontSize(){
    txtSize = document.getElementById("fontsize").value
}

function deleteOld(){
    var tmp = undoList.pop();
    ctx.putImageData(tmp, 0, 0);
    undoList.push(tmp);
}
function clearCanvas(){
    var r = confirm("刪掉就回不去了瑞凡");
    if(r){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        undoList=[];
        redoList=[];        
    }

}
