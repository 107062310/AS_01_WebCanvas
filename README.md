# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | N         |


---

### How to use 
#### 整體
![](https://i.imgur.com/a9tTXQR.png)

#### 工具

  畫面最右側為工具欄。由上至下分別是 :

* ##### 調色盤
        使用<input>內建調色盤，可改變筆刷及文字的顏色
* ##### 筆刷大小
        拖動滑桿以調整筆刷大小
* ##### 文字設定
        可以選擇文字的內容、大小、及字體
* ##### 清除畫布 Clear
        點擊可以清除畫布，會先彈出訊息詢問是否刪除，確認後無法復原成清除前的樣子
* ##### 復原 Undo
        點擊可以回到上一步
* ##### 重做 Redo
        點擊可以回到下一步
* ##### 保存 Save
        點擊可以直接下載整個畫布
* ##### 上傳圖片 Upload Image
        點擊可以選擇欲上傳的圖片，圖片上傳後會填滿整個畫布


#### 繪圖

畫面最左側可以選擇繪製的模式。由上到下分別是 :

* ##### 普通筆刷 Brush
        滑鼠落下再放開之後，canvas變得更美了
        
* ##### 橡皮擦 Eraser
        能夠抹除一切瑕疵
        
* ##### 直線 Line
        mousedown : 固定線的一端
        mousemove : 滑鼠目前座標為線的另一端
        mouseup   : 一條線完成了
        
* ##### 圓形 Circle
        mousedown : 固定圓的一端
        mousemove : 以滑鼠目前座標和mousedown時位置的距離為直徑畫圓
        mouseup   : 一個圓完成了
        
* ##### 矩形 Rectangle
        mousedown : 固定矩形的一角
        mousemove : 滑鼠目前座標為mousedown時位置的對角
        mouseup   : 一個矩形完成了
        
* ##### 三角形 Triangle
        mousedown : 固定三角形的一個頂點
        mousemove : 滑鼠目前座標為三角形的另一個頂點，並且以mousedown時的x座標為對稱軸得到最後一個頂點
        mouseup   : 一個三角形完成了
        
* ##### 文字 Text
        Text Input: 先在外面的文字輸入方塊輸入文字、選擇字體及大小
        mousedown : 選擇文字的起始位置
        mousemove : 拖動文字方塊
        mouseup   : 一段文字完成了
        
* ##### 彩虹筆刷 Rainbow Brush
        和普通筆刷一樣的用法，畫出來的顏色卻是漸變彩虹
 ___
### Function description
* #### 畫圖
        首先，把落筆時x、y的起始位置記起來，記做laxtX、lastY
        接著，從(lastX、lastY)畫一條線到滑鼠現在的位置
        再將lastX、lastY更新成目前的滑鼠位置
        這樣筆刷看起來就是隨著滑鼠移動而畫圖的了!


* #### 復原/重做
        首先創造兩個Array分別存undo和redo會用到的畫布
        在畫圖時，mousedown時把當前的畫布push到undoList，這樣存的會是這一筆還沒畫之前的狀態，redoList則保持清空
        
        在undo時，先把目前的畫布存到redoList中
        接著清空畫布，把undoList最後一張圖畫到畫布上，undo就完成了!
        
        而在redo時，先把目前的畫布存回undoList
        接著清空畫布，把redoList最後一張圖畫到畫布上，redo就完成了!
        
* #### 清除移動時的路徑
    #####        在繪製圖形的時候，滑鼠移動時，會畫出圖形當時的位置，但是如果不把這些不是終點畫出來的圖形刪掉，整個畫布就會充滿不需要的雜線。因此，在繪製圖形時，應該要邊畫新的圖形，邊把移動路徑上舊的圖形擦掉。
        這時候我們會使用到undoList的最後一張圖，裡面存的是畫這個圖形前的畫布
        接著，在繪製新的圖形前，先把undoList最後一張圖pop出來並重新繪製到畫布上
        因為這個畫面還會用來清空下一個路徑上的圖形，因此畫好之後還要push回undoList
        最後，才畫出新的圖形，這樣一來，繪製圖形時的移動路徑就不會留在畫布上了!
        
        
        
        
        
* #### 彩虹筆刷的漸變之謎
        使用彩虹筆刷時，筆刷的顏色會改成使用css的"hsl()" function。hsl相當於色相環的概念
        參數包含:色相環的角度(hue)、飽和度(saturation)及明度(lightness)
        
        hue:從0(red)開始，隨著畫筆移動累加到360(回到red)之後再歸零重新累加
        
        saturation:100%，才會顯得彩虹的繽紛
        
        lightness:50%
        
        
* #### 音樂播放器 Music Player
        畫面右上角有個音樂播放器，如果想要放鬆身心靈，點擊可以播放音樂，在點擊停止前會自動循環
___
### Gitlab page link

"https://107062310.gitlab.io/AS_01_WebCanvas"
___


<style>
table th{
    width: 100%;
}
</style>